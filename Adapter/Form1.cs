﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Adapter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Add("Mastercard");
            comboBox1.Items.Add("PayPal");
            comboBox1.Items.Add("Visa");
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IPay pay = null;

            if (comboBox1.Text.Equals("PayPal"))
                pay = new PayPal();
            else if (comboBox1.Text.Equals("Mastercard"))
                pay = new MasterCard();
            else if (comboBox1.Text.Equals("Visa"))
                pay = new VisaAdapter();

            MessageBox.Show(pay?.DoPay(10.33m));           
        }
    }
}
