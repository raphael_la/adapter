﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    class MasterCard : IPay
    {
        public string DoPay(decimal betrag)
        {
            return "Bezahlt mit Mastercard " + betrag.ToString();
        }
    }
}
