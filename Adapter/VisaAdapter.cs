﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    class VisaAdapter : IPay
    {
        public string DoPay(decimal betrag)
        {
            return new Visa.VisaPay().DoPayment(betrag, string.Empty);
        }
    }
}
